using Auto_Enrollment_Caliper.Pages.Components;
using Auto_Enrollment_Caliper.Pages.Forms.FormField;
using Auto_Enrollment_Caliper.SharedComponents;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Auto_Enrollment_Caliper.Pages
{
    public class EditAssociationForm : PageModel
    {
        public readonly PageBannerModel Banner = new PageBannerModel(
            "edit-association-form-page-banner",
            "Edit Registration Form"
        );

        public readonly FormFieldModel[] AssociationSettings = new []
        {
            new FormFieldModel(
                "logoURL",
                "https://cdn1.onlineaccess1.com/cdn/depot/3032_19/2456/9d7d183849ff7a7115f8be52462ecfa0/assets/images/logos/logo_large-e0f3d2618917cb665979562a9e821f09.png",
                "Logo URL",
                "text",
                "col-12",
                true
            ),
            new FormFieldModel(
                "apiURL",
                "https://test.q2api.com/v1/",
                "API URL",
                "text",
                "col-12",
                true
            ),
            new FormFieldModel(
                "apiKey",
                "ehar0BPWaU5N05TKZUpnE8nS4HlfLGfn50RD6nD3",
                "API Key",
                "text",
                "col-12",
                true
            ),
        };
        
        public readonly ChipsInputModel AssociationAdmins = new ChipsInputModel(
            "admins", 
            "Association Admins",
            "col-12",
            new[]
            {
                "Vasiliy.Ivanov@gmail.com",
                "Ivan.Petrov@gmail.com",
                "Stepan.Sidorov@gmail.com"
            }
        );
        
        public readonly DropdownInputModel TestDrodpownInput = new DropdownInputModel(
            "testInput",
            "Test Input",
            "col-12",
            new[]
            {
                "First",
                "Second",
                "Third"
            },
            1
        );

        public readonly FormFieldModel[] EditableFields = new[]
        {
            new FormFieldModel(
                "Address1",
                "",
                "Address",
                "text",
                "col-12 col-xl-6 col-lg-6",
                true,
                false,
                "Contact Information",
                "Your primary address",
                "/^[a-zA-Z0-9 '-,.]{2,200}$/",
                2,
                200,
                2
            ),
            new FormFieldModel(
                "FirstName",
                "",
                "First Name",
                "text",
                "col-12 col-xl-6 col-lg-6",
                true,
                true,
                "Contact Information",
                "Your first name",
                "/^[a-zA-Z0-9 '-,.]{2,200}$/",
                2,
                30,
                2
            )
        };
    }
}