using System;
using System.Linq;
using Auto_Enrollment_Caliper.Pages.Components;
using Auto_Enrollment_Caliper.Pages.Forms.FormField;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Auto_Enrollment_Caliper.SharedComponents;

namespace Auto_Enrollment_Caliper.Pages
{
    public class RegistrationForm : PageModel
    {
        public string[] Tabs = new[]
        {
            "General Information",
            "Contact Information",
            "Security Settings"
        };

        public FormFieldModel[] Fields = {
            new FormFieldModel(
                "Address1",
                "",
                "Address",
                "text",
                "col-12 col-xl-6 col-lg-6",
                false,
                true,
                "Contact Information",
                "Your primary address",
                "/^[a-zA-Z0-9 '-,.]{2,200}$/",
                2,
                200,
                1
            ),
            new FormFieldModel(
                "AddressType",
                "",
                "Address Type",
                "text",
                "col-12 col-xl-6 col-lg-6",
                true,
                true,
                "General Information",
                "Type of address",
                null,
                null,
                null,
                1
            ),
            new FormFieldModel(
                "State",
                "",
                "State",
                "text",
                "col-12 col-xl-6 col-lg-6",
                true,
                true,
                "Contact Information",
                "Your home state",
                null,
                null,
                null,
                1
            ), 
            new FormFieldModel(
                "FirstName",
                "",
                "First Name",
                "text",
                "col-12 col-xl-6 col-lg-6",
                true,
                true,
                "General Information",
                "Your first name",
                "/^[a-zA-Z '-]{2,30}$/",
                2,
                30,
                1
            ),
            new FormFieldModel(
                "Password",
                "",
                "Password",
                "password",
                "col-12 col-xl-6 col-lg-6",
                true,
                true,
                "Security Settings",
                "Your Q2 Password",
                "",
                2,
                30,
                1
            )
        };
        
        public DropdownInputModel StateSelector = new DropdownInputModel(
            "State",
            "State",
            "col-1 col-lg-6 col-xl-6",
            new string[]
            {
                "Alabama (AL)",
                "Alaska (AK)",
                "Arizona (AZ)",
                "Arkansas (AR)",
                "California (CA)",
                "Colorado (CO)",
                "Connecticut (CT)",
                "Delaware (DE)",
                "Florida (FL)",
                "Georgia (GA)",
                "Hawaii (HI)",
                "Idaho (ID)",
                "Illinois (IL)",
                "Indiana (IN)",
                "Iowa (IA)",
                "Kansas (KS)",
                "Kentucky (KY)",
                "Louisiana (LA)",
                "Maine (ME)",
                "Maryland (MD)",
                "Massachusetts (MA)",
                "Michigan (MI)",
                "Minnesota (MN)",
                "Mississippi (MS)",
                "Missouri (MO)",
                "Montana (MT)",
                "Nebraska (NE)",
                "Nevada (NV)",
                "New Hampshire (NH)",
                "New Jersey (NJ)",
                "New Mexico (NM)",
                "New York (NY)",
                "North Carolina (NC)",
                "North Dakota (ND)",
                "Ohio (OH)",
                "Oklahoma (OK)",
                "Oregon (OR)",
                "Pennsylvania (PA)",
                "Rhode Island (RI)",
                "South Carolina (SC)",
                "South Dakota (SD)",
                "Tennessee (TN)",
                "Texas (TX)",
                "Utah (UT)",
                "Vermont (VT)",
                "Virginia (VA)",
                "Washington (WA)",
                "West Virginia (WV)",
                "Wisconsin (WI)",
                "Wyoming (WY)"
            }
        ); 
        
        public DropdownInputModel AddressTypeSelector = new DropdownInputModel(
            "AddressType",
            "Address Type",
            "col-1 col-lg-6 col-xl-6",
            new string[]
            {
                "Residential", "Postal", "Vacation", "Home", "Other", "Business", "BillPayee"
            }
        );

        public readonly string loginURL = "https://secure3.onlineaccess1.com/CapitalFarmCreditBankOnline_Test/uux.aspx#login";

        public readonly string logoURL = "https://wus2-prod-fp7mmny5s2xxi.azureedge.net/f/img/40/cfc-logo.png";

        public readonly PageBannerModel Banner = new PageBannerModel(
        "registration-form-page-banner",
        "Fill the Registration Form"
        );
    }
}