﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Auto_Enrollment_Caliper.SharedComponents;

namespace Auto_Enrollment_Caliper.Pages
{
    public class IndexModel : PageModel
    {
        public readonly PageBannerModel Banner = new PageBannerModel("home-page-banner", "Welcome!");

        public readonly AssociationCardModel[] CardModels = new []
        {
            new AssociationCardModel(
                "002",
                "https://wus2-prod-fp7mmny5s2xxi.azureedge.net/f/img/40/cfc-logo.png",
                "Capital Farm Credit"
            ),
            new AssociationCardModel(
                "001",
                "https://cdn1.onlineaccess1.com/cdn/depot/3032_19/2456/9d7d183849ff7a7115f8be52462ecfa0/assets/images/logos/logo_large-e0f3d2618917cb665979562a9e821f09.png",
                "Alabama Farm"
            )
        };
    }
}