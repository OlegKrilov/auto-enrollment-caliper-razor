using System;
using System.Threading.Tasks;

namespace Auto_Enrollment_Caliper.SharedComponents
{
    public class SwitchModel
    {
        public string name { get; }

        public bool state { get; }

        public SwitchModel(string name, bool state = false)
        {
            this.name = name;
            this.state = state;
        }
    }
}