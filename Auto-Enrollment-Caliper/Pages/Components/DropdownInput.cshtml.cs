namespace Auto_Enrollment_Caliper.Pages.Components
{
    public class DropdownInputModel
    {
        public readonly string name;

        public readonly string className;

        public readonly string Label; 

        public readonly string[] items;

        public readonly int selection;

        public DropdownInputModel(
            string name, 
            string label, 
            string className = "col-12 col-lg-6 col-xl-6", 
            string[] items = null,
            int selection = 0
        )
        {
            this.name = name;
            this.Label = label;
            this.className = className;
            this.items = items ?? new string[0];
            this.selection = selection;
        }
    }
}