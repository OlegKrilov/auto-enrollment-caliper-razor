using System;

namespace Auto_Enrollment_Caliper.SharedComponents
{
    public class PageBannerModel
    {
        public readonly string componentId;

        public readonly string label;

        public readonly string[] images;

        public PageBannerModel(string componentId, string label = "")
        {
            this.componentId = componentId;
            this.label = label;
            this.images = new string[7]
            {
                "bg_1.jpg",
                "bg_2.jpg",
                "bg_3.jpg",
                "bg_4.jpg",
                "bg_5.jpg",
                "bg_6.jpg",
                "bg_7.jpg"
            };
        }
    }
}