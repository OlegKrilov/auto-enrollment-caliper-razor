using Auto_Enrollment_Caliper.SharedComponents;

namespace Auto_Enrollment_Caliper.Pages.Forms.FormField
{
    public class FormFieldModel
    {
        public readonly dynamic associationId;

        public readonly string name;

        public readonly string value;

        public readonly string label;

        public readonly string folder;

        public readonly string className;

        public readonly string type;

        public readonly dynamic format;

        public readonly dynamic min;

        public readonly dynamic max;

        public readonly string text;

        public SwitchModel required;

        public SwitchModel visible;

        public bool collapsed;

        public FormFieldModel(
            string name,
            string value = "",
            string label = "",
            string type = "text",
            string className = "col-12 col-xl-6 col-lg-6",
            bool required = false,
            bool visible = true,
            string folder = "",
            string text = "",
            dynamic format = null,
            dynamic min = null,
            dynamic max = null,
            dynamic associationId = null
        )
        {
            this.name = name;
            this.value = value;
            this.label = label;
            this.folder = folder;
            this.type = type;
            this.className = className;
            this.text = text;
            this.required = new SwitchModel("required", required);
            this.visible = new SwitchModel("visible", visible);
            this.format = format ?? "";
            this.min = min ?? 0;
            this.max = max ?? 0;
            this.associationId = associationId;
            this.collapsed = true;
        }
    }
}

