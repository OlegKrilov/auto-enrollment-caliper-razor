using System.Reflection.Emit;

namespace Auto_Enrollment_Caliper.Pages.Components
{
    public class ChipsInputModel
    {
        public readonly string name;

        public readonly string className;

        public readonly string Label; 

        public readonly string[] items;

        public ChipsInputModel(
            string name, 
            string label, 
            string className = "col-12 col-lg-6 col-xl-6", 
            string[] items = null
        )
        {
            this.name = name;
            this.Label = label;
            this.className = className;
            this.items = items ?? new string[0];
        }
    }
}