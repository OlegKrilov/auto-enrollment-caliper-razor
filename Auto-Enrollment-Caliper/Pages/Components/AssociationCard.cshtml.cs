namespace Auto_Enrollment_Caliper.SharedComponents
{
    public class AssociationCardModel
    {

        public readonly string associationId;

        public readonly string associationLogo;

        public readonly string associationName;

        public AssociationCardModel(string associationId, string associationLogo, string associationName)
        {
            this.associationId = associationId;
            this.associationLogo = associationLogo;
            this.associationName = associationName;
        }
    }
}
