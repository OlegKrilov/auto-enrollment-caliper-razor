const
  IS_SELECTED = 'isSelected',
  IS_HIDDEN = 'isHidden',
  IS_READY = 'isReady',
  HAS_ERRORS = 'hasErrors';

const
  REGISTRATION_FORM_TAB = 'aec-registration-form-tab',
  REGISTRATION_FORM = 'aec-registration-form',
  REGISTRATION_FORM_DATA = 'aec-registration-form-data',
  REGISTRATION_FORM_FIELD_DATA = 'aec-field-data',
  REGISTRATION_FORM_HEADER = 'aec-registration-form-header',
  REGISTRATION_FORM_TITLE = 'aec-registration-form-title',
  REGISTRATION_FORM_ERRORS = 'aec-registration-form-errors',
  INPUT_FIELD_CORE = 'aec-form-field-core';

const
  DROPDOWNS = ['AddressType', 'State'];

const
  fields = {},
  tabs = {};

const
  onFormSubmit = (e) => {
    Object.keys(tabs).forEach(key => tabs[key].clearErrors().validate());
    setErrors();
    
    if (Object.keys(tabs).some(key => tabs[key].hasErrors)) {
      alert('Please, check your form!');
    } else {
      document.querySelector(`.${REGISTRATION_FORM}`).submit();
    }
  },

  changeTab = (tabName = '', state = false) => {
    const
      tabNames = Object.keys(tabs),
      form = document.querySelector(`.${REGISTRATION_FORM}`),
      header = document.querySelector(`.${REGISTRATION_FORM_HEADER}`),
      title = document.querySelector(`.${REGISTRATION_FORM_TITLE}`);

    tabNames.forEach(_tabName => tabs[_tabName].toggle(state ? _tabName === tabName : false));
    header.textContent = state ? tabName : '';

    state ?
      title.classList.add(IS_HIDDEN) :
      title.classList.remove(IS_HIDDEN);

    tabs[tabNames[tabNames.length - 1]].isSelected ?
      form.classList.add(IS_READY) :
      form.classList.remove(IS_READY);

    setErrors();
  },

  getSelectedTab = () => tabs[Object.keys(tabs).find(key => tabs[key].isSelected)] || null,

  getTabByIndex = (index) => tabs[Object.keys(tabs).find(key => tabs[key].index === index)] || null,

  deselectAll = () => changeTab(),

  stepBack = () => {
    const
      selectedTab = getSelectedTab();

    if (selectedTab === null) window.location.href = '/';
    else {
      const
        nextTab = getTabByIndex(selectedTab.index - 1);

      nextTab ? changeTab(nextTab.name, true) : changeTab();
    }
  },

  stepForward = () => {
    const
      selectedTab = getSelectedTab(),
      nextTab = getTabByIndex(selectedTab ? selectedTab.index + 1 : 0);

    nextTab ? changeTab(nextTab.name, true) : onFormSubmit();
  },
  
  setErrors = () => {
    const
      selectedTab = getSelectedTab(),
      errorsMarkup = selectedTab ? selectedTab.getErrors() : '';
  
    document.querySelector(`.${REGISTRATION_FORM_ERRORS}`).innerHTML = errorsMarkup ? 
      `${errorsMarkup} <div class="aec-registration-form-errors-clear" onclick="clearTabErrors()">X</div>` : '';
  },
  
  clearTabErrors = () => {
    const
      selectedTab = getSelectedTab();
    
    selectedTab && selectedTab.clearErrors();
    setErrors();
  };

document.addEventListener('DOMContentLoaded', () => {
  Array.from(document.querySelectorAll(`.${REGISTRATION_FORM_TAB}`))
    .map((elem, index) => Object.assign(tabs, {
      [elem.textContent.trim()]: new RegistrationFormTab(elem, index)
    }));
  
  Array.from(document
    .querySelector(`.${REGISTRATION_FORM_DATA}`)
    .querySelectorAll(`.${REGISTRATION_FORM_FIELD_DATA}`)
  )
    .forEach(dataContainer => {
      const
        datum = JSON.parse(dataContainer.textContent),
        id = datum.name;
      
      tabs[datum['folder']].addField(id, new RegistrationFormField(document.querySelector(`#aec-form-field-${id}`), datum).toggle());
    });
});

function RegistrationFormTab (elem, index) {
  const
    model = this,
    tabName = elem.textContent.trim();

  const
    relatedFields = {},
    errorsList = {};

  let
    isSelected = false;

  Object.defineProperties(model, {
    name: {
      get: () => tabName
    },
    index: {
      get: () => index
    },
    fields: {
      get: () => relatedFields
    },
    toggle: {
      value: state => {
        isSelected = !!state;
        isSelected ? elem.classList.add(IS_SELECTED) : elem.classList.remove(IS_SELECTED);
        return model.toggleFields();
      }
    },
    addField: {
      value: (fieldId, fieldModel) => {
        relatedFields[fieldId] = fieldModel;
        return this;
      }
    },
    toggleFields: {
      value: () => {
        Object.keys(relatedFields).forEach(key => relatedFields[key].toggle(isSelected));
        return this;
      }
    },
    clearErrors: {
      value: () => {
        Object.keys(errorsList).forEach(key => delete errorsList[key]);
        return model.markAsValid(true);
      }
    },
    getErrors: {
      value: () => Object.keys(errorsList).map(key => `<div class="aec-registration-form-error">${errorsList[key]}</div>`).join('')
    },
    validate: {
      value: () => {
        model.clearErrors();
        
        const
          errors = Object.keys(relatedFields)
            .map(key => relatedFields[key].validate())
            .filter(err => err);
        
        errors.forEach(err => Object.assign(errorsList, err));
        
        return model.markAsValid(!errors.length);
      }
    },
    markAsValid: {
      value: (state = false) => {
        state ?
          elem.classList.remove(HAS_ERRORS) :
          elem.classList.add(HAS_ERRORS);
        
        return model;
      }
    },
    isSelected: {
      get: () => isSelected
    },
    hasErrors: {
      get: () => !!Object.keys(errorsList).length
    }
  });
}

function RegistrationFormField (elem, datum) {
  const 
    model = this,
    coreElem = elem.querySelector(`.${INPUT_FIELD_CORE}`);
  
  coreElem.addEventListener('input', () => datum.value = coreElem.value);
  
  if (datum.format) {
    datum.format = datum.format
      .replace(/^\//, '')
      .replace(/\/$/, '');
  }
  
  Object.defineProperties(model, {
    name: {
      get: () => datum.name,
    },
    toggle: {
      value: (state = false) => {
        state ? elem.classList.remove(IS_HIDDEN) : elem.classList.add(IS_HIDDEN);
        return model;
      }
    },
    setValue: {
      value: value => {
        coreElem.value = value;
        return model;
      }
    },
    model: {
      value: DROPDOWNS.includes(datum.name) ? new DropdownInput(elem, {
        onChange: val => datum.value = val
      }) : null
    },
    validate: {
      value: () => {
        const
          fieldName = datum.name,
          currentValue = coreElem.value.trim() || '',
          currentLen = currentValue ? currentValue.length : 0,
          label = `<b>${datum.label}</b>`;
        
        if (datum.required) {
          if (!currentValue || !currentLen) return {[fieldName]: `${label}<span> is required!</span>`};
        }
        
        if (datum.min) {
          if (currentValue && (currentLen < datum.min)) return {[fieldName]: `${label}<span> is too short. Should has at least ${datum.min} symbols.</span>`};
        }
        
        if (datum.max) {
          if (currentValue && (currentLen > datum.max)) return {[fieldName]: `${label}<span> is too long. Should has less then ${datum.max} symbols.</span>`};
        }
        
        if (datum.format) {
          if (currentValue && !(new RegExp(datum['format'])).test(currentValue)) return {[fieldName]: `${label}<span> is in the incorrect format.</span>`};
        }

        return null;
      }
    }
  });
}
