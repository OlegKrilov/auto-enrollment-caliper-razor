function PageBanner (elem, events) {
  const
    IS_READY = 'isReady',
    IS_SELECTED = 'isSelected',
    LOCAL_STORAGE_KEY = '_img';
  
  const
    model = this;
  
  const
    images = Array.from(elem.querySelectorAll('.aec-page-banner-image'))
  
  let
    selection = Math.floor(localStorage.getItem(LOCAL_STORAGE_KEY) || 0),
    timer = null;
  
  const
    refreshView = () => {
      images.forEach((image, i) => (i + 1) === selection ?
        image.classList.add(IS_SELECTED) :
        image.classList.remove(IS_SELECTED)
      );
      
      return model;
    };
  
  Object.defineProperties(model, {
    select: {
      value: index => {
        selection = index;
        localStorage.setItem(LOCAL_STORAGE_KEY, selection);
        return refreshView();
      }
    },
    selectNext: {
      value: () => {
        const
          nextIndex = selection + 1;
        
        return model.select(nextIndex > images.length ? 1 : nextIndex);
      }
    },
    selectPrevious: {
      value: () => {
        const 
          nextIndex = selection - 1;
        return model.select(nextIndex < 1 ? images.length : nextIndex);
      }
    },
    run: {
      value: (delay = null) => {
        model.stop();
        timer = setTimeout(() => model.selectNext().run(), delay === null ? 10000 : delay);
        return model;
      }
    },
    stop: {
      value: () => {
        if (timer) clearTimeout(timer);
        timer = null;
        return model;
      }
    },
    init: {
      value: () => {
        setTimeout(() => elem.classList.add(IS_READY), 500);
        return model.selectNext().run(0)
      }
    }
  });
}

