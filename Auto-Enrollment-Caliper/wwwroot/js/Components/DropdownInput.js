function DropdownInput (elem, events = {}) {
  const
    IS_OPENED = 'isOpened',
    IS_SELECTED = 'isSelected',
    IS_REVERSED = 'isReversed';
  
  const
    model = this;
  
  const
    wrapper = elem.querySelector('.aec-dropdown-input'),
    header = wrapper.querySelector('.aec-dropdown-input-header'),
    body = wrapper.querySelector('.aec-dropdown-input-body'),
    coreInput = wrapper.querySelector('.aec-dropdown-core');
  
  let 
    selection = 0,
    isOpened = false,
    itemsList = [];
  
  const
    _globalClickEvent = (e) => {
      let
        {target} = e,
        clickedInside = false;
      
      while (target) {
        if (target === elem) {
          target = null;
          clickedInside = true;
        } else target = target.parentNode || null;
      }
      
      if (!clickedInside) model.toggle(false);
    },
    
    _refreshView = () => {
      document.removeEventListener('click', _globalClickEvent);
    
      const
        selectedIndex = selection - 1;
      
      if (isOpened) {
        body.innerHTML = itemsList.map((str, i) => `
          <div class="aec-dropdown-input-body-item ${i === selectedIndex ? IS_SELECTED : ''}">${str}</div>
        `).join('');
        
        Array.from(body.querySelectorAll(`.aec-dropdown-input-body-item`)).forEach(
          (item, i) => item.addEventListener('click', () => {
            if (i !== selectedIndex) {
              model.toggleItem(i + 1);
              model.toggle(false);
            }
          })
        );
        
        elem.getBoundingClientRect().top > (window.innerHeight * .5) ?
          wrapper.classList.add(IS_REVERSED) :
          wrapper.classList.remove(IS_REVERSED);

        document.addEventListener('click', _globalClickEvent);
        wrapper.classList.add(IS_OPENED)
      } else {
        document.removeEventListener('click', _globalClickEvent);
        wrapper.classList.remove(IS_OPENED)
      }

      header.innerHTML = `<span class="aec-dropdown-input-header-label">${selection ? itemsList[selectedIndex] : ''}</span>`;
      coreInput.value = selection ? itemsList[selectedIndex] : '';
      return model;
    };
  
  Object.defineProperties(model, {
    toggle: {
      value: (state = null) => {
        isOpened = state === null ? !isOpened : !!state;
        return _refreshView();
      }
    },
    addItems: {
      value: (items) => {
        (Array.isArray(items) ? items : [items]).forEach(item => itemsList.push(item));
        return _refreshView();
      }
    },
    toggleItem: {
      value: (index) => {
        selection = index;
        _refreshView();
        events && typeof events.onChange === "function" && events.onChange(coreInput.value);
        return model;
      }
    },
    clearSelection: () => model.toggleItem(0)
  });

  header.addEventListener('click', () => {
    model.toggle();
  });

  model.addItems(Array.from(elem.querySelectorAll('.aec-field-data-item')).map(_elem => _elem.textContent));
  
  model.toggleItem(+elem.querySelector('.aec-field-data-selection').textContent);
}

