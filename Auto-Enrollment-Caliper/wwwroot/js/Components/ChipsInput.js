function ChipsInput (elem, events = {}) {
  const
    ENTER_KEY = 13,
    BACKSPACE_KEY = 8,
    IS_EMPTY = 'isEmpty';
  
  const
    model = this;
  
  const
    wrapper = elem.querySelector('.aec-chips-input'),
    contentView = elem.querySelector('.aec-chips-input-content'),
    itemsView = contentView.querySelector('.aec-chips-input-content-items'),
    interfaceInput = contentView.querySelector('.aec-chips-input-content-core'),
    coreInput = elem.querySelector('.aec-chips-core');
  
  let 
    itemsList = [];
  
  const
    _refreshView = () => {
      itemsView.innerHTML = itemsList.map(str => `
          <div class="aec-chips-input-content-item">
              <span class="aec-chips-input-content-item-label">${str}</span>
              <div class="aec-chips-input-content-item-remove"></div>
          </div>
      `).join('');

      Array.from(itemsView.querySelectorAll('.aec-chips-input-content-item')).forEach((item, i) => {
        item.querySelector('.aec-chips-input-content-item-remove').addEventListener('click', () => {
          model.removeItem(i);
          _refreshView();
        });
      });
      
      itemsList.length ?
        wrapper.classList.remove(IS_EMPTY) :
        wrapper.classList.add(IS_EMPTY);
      
      coreInput.value = itemsList.join(';');
      
      return model;
    };
  
  Object.defineProperties(model, {
    addItems: {
      value: items => {
        (Array.isArray(items) ? items : [items]).forEach(item => itemsList.push(item));
        return _refreshView();
      }
    },
    removeItem: {
      value: index => {
        itemsList = itemsList.filter((str, ii) => ii !== index);
        return _refreshView();
      }
    },
    clearItems: {
      value: () => {
        itemsList.length = 0;
        return _refreshView();
      }
    }
  });

  interfaceInput.addEventListener('keydown', (e) => {
    const
      key = e.which,
      currentValue = (interfaceInput.value).trim();

    if (currentValue && key === ENTER_KEY) {
      model.addItems(currentValue);
      interfaceInput.value = '';
      _refreshView();
    }
    else if (!currentValue && itemsList.length && key === BACKSPACE_KEY) {
      model.removeItem(itemsList.length - 1);
      _refreshView();
    }
  });
  
  model.addItems(Array.from(elem.querySelectorAll('.aec-field-data-item')).map(_elem => _elem.textContent));
}