const
  EDITABLE_FIELD = 'aec-editable-field',
  EDITABLE_FIELD_DATA = 'aec-field-data',
  MAIN_FORM = 'aec-edit-association-form',
  ASSOCIATION_GLOBAL_SETTINGS_FORM = 'aec-association-global-settings-form',
  IS_EXPANDED = 'isExpanded',
  IS_REQUIRED = 'required',
  IS_VISIBLE = 'visible',
  TEXT = 'text',
  FORMAT = 'format',
  LOGO = 'logoURL',
  API_URL = 'apiURL',
  API_KEY = 'apiKey',
  ASSOCIATION_ADMINS = 'admins';

const
  associationSettingsFieldsList = {},
  fieldsList = {};

const
  toggleClass = (elem, className, state) => {
    try {
      state ? elem.classList.add(className) : elem.classList.remove(className);
    } catch (err) {
      console.log(err);
    }
  },

  toggleEditableField = (fieldId, state) => {
    try {
      const
        fields = Array.from(document.querySelectorAll(`.${EDITABLE_FIELD}`));

      fields.forEach(field => toggleClass(field, IS_EXPANDED, state ? field.id === fieldId : false));
    } catch (err) {
      console.log(err);
    }
  },

  submitData = () => {
    const
      associationSettingsForm = document.querySelector(`.${ASSOCIATION_GLOBAL_SETTINGS_FORM}`),
      hiddenForm = document.querySelector(`.${MAIN_FORM}`);

    const
      data = Object.assign(
        {
          fields: JSON.stringify(Object.keys(fieldsList).map(key => fieldsList[key].getData()))
        },
        [LOGO, API_KEY, API_URL, ASSOCIATION_ADMINS].reduce(
          (fields, prop) => Object.assign(
            fields, {
              [prop]: associationSettingsForm.querySelector(`[name="${prop}"]`).value
            }
          ), {}
        )
      );
    
    console.log(data);

    Object.keys(data).forEach(key => hiddenForm['elements'][key].value = data[key]);
    
    // hiddenForm.submit();
  },

  getAllFieldsByName = (root, name) => Array.from(root.querySelectorAll(`[name="${name}"]`)),

  sendForm = (e) => {
    e.preventDefault();
    console.log(e);
  };

document.addEventListener('DOMContentLoaded', (e) => {
  Array.from(document.querySelectorAll(`.${EDITABLE_FIELD}`)).forEach(
    elem => fieldsList[elem.id] = new EditableField(elem)
  );
  
  Array.from(document.querySelectorAll(`#aec-form-field-admins`)).forEach(elem => new ChipsInput(elem));
  
  Array.from(document.querySelectorAll('#aec-form-field-testInput')).forEach(elem => new DropdownInput(elem));
});

function EditableField (elem) {
  const
    model = this,
    elemId = elem.id,
    datum = JSON.parse(elem.querySelector(`.${EDITABLE_FIELD_DATA}`).textContent);

  Object.defineProperties(model, {
    toggle: {
      value: state => toggleClass(elem, IS_EXPANDED, state)
    },
    getData: {
      value: () => ({...datum})
    }
  });

  [IS_REQUIRED, IS_VISIBLE].forEach(prop => {
    const
      checkboxes = getAllFieldsByName(elem, prop);

    checkboxes.forEach((checkboxElem, i) => {
      checkboxElem.addEventListener('change', () => {
        checkboxes.forEach((_checkboxElem, _i) => {
          if (_i !== i) _checkboxElem.checked = checkboxElem.checked;
        });
        datum[prop] = checkboxElem.checked;
      });
    });
  });

  [TEXT, FORMAT].forEach(prop => {
    const
      textInputs = getAllFieldsByName(elem, prop);

    textInputs.forEach((textInput) => textInput.addEventListener('input', () =>
      datum[prop] = textInput.value
    ));
  });
}

